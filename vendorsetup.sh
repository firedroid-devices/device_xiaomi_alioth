
# Device Common Tree
echo 'Cloning Devie Common tree [1/4]'
git clone git@gitlab.com:firedroid-devices/device_xiaomi_sm8250-common.git -b 14 device/xiaomi/sm8250-common

# Vendor Tree
echo 'Cloning Vendor tree [2/4]'
git clone git@gitlab.com:firedroid-devices/vendor_xiaomi_alioth.git -b 14 vendor/xiaomi/alioth

# Vendor Common Tree
echo 'Cloning Vendor Common tree [3/4]'
git clone git@gitlab.com:firedroid-devices/vendor_xiaomi_sm8250-common.git -b 14 vendor/xiaomi/sm8250-common

# Leica Cam Repo
echo 'Cloning leica Cam Repo [4/4]'
git clone git@gitlab.com:firedroid/vendor_xiaomi_camera.git -b stable vendor/xiaomi/camera